USE `cs3230f17e`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryDescription` varchar(45) NOT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Chair'),(2,'Ottoman'),(3,'Recliner'),(4,'Couch'),(5,'Bed'),(6,'Futon'),(7,'Desk'),(8,'Table'),(9,'Bookcase');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `EmployeeID` int(11) NOT NULL AUTO_INCREMENT,
  `IsAdministrator` tinyint(4) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Person_PersonID` int(11) NOT NULL,
  PRIMARY KEY (`EmployeeID`),
  KEY `fk_Employee_Person1_idx` (`Person_PersonID`),
  CONSTRAINT `fk_Employee_Person1` FOREIGN KEY (`Person_PersonID`) REFERENCES `person` (`PersonID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,1,'*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19',1),(2,0,'*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19',2);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `furniture`
--

DROP TABLE IF EXISTS `furniture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `furniture` (
  `ItemNumber` int(11) NOT NULL,
  `Style_StyleID` int(11) NOT NULL,
  `Category_CategoryID` int(11) NOT NULL,
  `Rate` decimal(10,2) DEFAULT NULL,
  `SKU` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ItemNumber`),
  UNIQUE KEY `ItemNumber_UNIQUE` (`ItemNumber`),
  KEY `fk_Inventory_Style1_idx` (`Style_StyleID`),
  KEY `fk_Inventory_Category1_idx` (`Category_CategoryID`),
  CONSTRAINT `fk_Inventory_Category1` FOREIGN KEY (`Category_CategoryID`) REFERENCES `category` (`CategoryID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Inventory_Style1` FOREIGN KEY (`Style_StyleID`) REFERENCES `style` (`StyleID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `furniture`
--

LOCK TABLES `furniture` WRITE;
/*!40000 ALTER TABLE `furniture` DISABLE KEYS */;
INSERT INTO `furniture` VALUES (1,1,1,50.00,'A1'),(2,1,1,55.00,'B1'),(3,2,2,20.00,'A2'),(4,3,3,30.00,'A3'),(5,4,4,40.00,'A4'),(6,5,5,50.00,'A5'),(7,6,6,60.00,'A6'),(8,7,7,70.00,'A7'),(9,8,8,80.00,'A8'),(10,9,9,90.00,'A9'),(11,1,4,20.00,'A10');
/*!40000 ALTER TABLE `furniture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `MemberID` int(11) NOT NULL AUTO_INCREMENT,
  `Street` varchar(45) NOT NULL,
  `City` varchar(45) NOT NULL,
  `State` varchar(45) NOT NULL,
  `Zip` varchar(45) NOT NULL,
  `Phone` varchar(45) NOT NULL,
  `Person_PersonID` int(11) NOT NULL,
  PRIMARY KEY (`MemberID`),
  KEY `fk_Member_Person1_idx` (`Person_PersonID`),
  CONSTRAINT `fk_Member_Person1` FOREIGN KEY (`Person_PersonID`) REFERENCES `person` (`PersonID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'Street One','City One','ID','12345','1234567890',3),(2,'Street Two','City Two','GA','12345','1234567890',4),(3,'Street Three','City Three','ME','12345','1234567893',5),(4,'Street Four','City Four','CA','54321','9987654321',6);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `PersonID` int(11) NOT NULL AUTO_INCREMENT,
  `FName` varchar(45) NOT NULL,
  `LName` varchar(45) NOT NULL,
  `SSN` varchar(45) NOT NULL,
  `Gender` varchar(45) NOT NULL,
  PRIMARY KEY (`PersonID`),
  UNIQUE KEY `SSN_UNIQUE` (`SSN`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'First','Last','123456789','M'),(2,'Second','Last','123456790','M'),(3,'Third','Last','123456791','Male'),(4,'Fourth','Last','123456792','Female'),(5,'Fifth','Last Second','123324451','Other'),(6,'Sixth','Last Sixth','987654321','Male');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `return`
--

DROP TABLE IF EXISTS `return`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `return` (
  `Transaction_has_Furniture_Transaction_TransactionID` int(11) NOT NULL,
  `Transaction_has_Furniture_Furniture_ItemNumber` int(11) NOT NULL,
  `Employee_EmployeeID` int(11) NOT NULL,
  `ReturnDate` date NOT NULL,
  PRIMARY KEY (`Transaction_has_Furniture_Transaction_TransactionID`,`Transaction_has_Furniture_Furniture_ItemNumber`),
  KEY `fk_Return_Employee1_idx` (`Employee_EmployeeID`),
  CONSTRAINT `fk_Return_Employee1` FOREIGN KEY (`Employee_EmployeeID`) REFERENCES `employee` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Return_Transaction_has_Furniture1` FOREIGN KEY (`Transaction_has_Furniture_Transaction_TransactionID`, `Transaction_has_Furniture_Furniture_ItemNumber`) REFERENCES `transaction_has_furniture` (`Transaction_TransactionID`, `Furniture_ItemNumber`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `return`
--

LOCK TABLES `return` WRITE;
/*!40000 ALTER TABLE `return` DISABLE KEYS */;
INSERT INTO `return` VALUES (1,1,1,'2017-11-27'),(1,2,1,'2017-11-27'),(1,3,1,'2017-11-27'),(2,1,1,'2017-11-27'),(2,2,1,'2017-11-27'),(2,3,1,'2017-11-27'),(3,4,1,'2017-11-27'),(3,5,1,'2017-11-27'),(3,11,1,'2017-11-27'),(4,6,1,'2017-11-27'),(4,7,1,'2017-11-27'),(4,8,1,'2017-11-27'),(5,1,2,'2017-11-28'),(5,3,2,'2017-11-28'),(5,4,2,'2017-11-28'),(6,1,2,'2017-11-28'),(6,2,2,'2017-11-28'),(7,1,2,'2017-11-28'),(8,2,2,'2017-11-28');
/*!40000 ALTER TABLE `return` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `style`
--

DROP TABLE IF EXISTS `style`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `style` (
  `StyleID` int(11) NOT NULL AUTO_INCREMENT,
  `StyleDescription` varchar(45) NOT NULL,
  PRIMARY KEY (`StyleID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `style`
--

LOCK TABLES `style` WRITE;
/*!40000 ALTER TABLE `style` DISABLE KEYS */;
INSERT INTO `style` VALUES (1,'Jacobean'),(2,'William and Mary'),(3,'Queen Anne'),(4,'Pennsylvania Dutch'),(5,'Chippendale'),(6,'Hepplewhite'),(7,'Federal'),(8,'Sheraton'),(9,'American Empire'),(10,'Victorian'),(11,'Arts and Craft'),(12,'Scandinavian Contemporary');
/*!40000 ALTER TABLE `style` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `TransactionID` int(11) NOT NULL AUTO_INCREMENT,
  `Member_MemberID` int(11) NOT NULL,
  `Employee_EmployeeID` int(11) NOT NULL,
  `TransactionDate` date NOT NULL,
  `DueDate` date NOT NULL,
  PRIMARY KEY (`TransactionID`),
  KEY `fk_Transaction_Member1_idx` (`Member_MemberID`),
  KEY `fk_Transaction_Employee1_idx` (`Employee_EmployeeID`),
  CONSTRAINT `fk_Transaction_Employee1` FOREIGN KEY (`Employee_EmployeeID`) REFERENCES `employee` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transaction_Member1` FOREIGN KEY (`Member_MemberID`) REFERENCES `member` (`MemberID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (1,1,1,'2017-11-27','2017-11-28'),(2,2,1,'2017-11-27','2017-11-28'),(3,1,1,'2017-11-27','2017-11-28'),(4,3,1,'2017-11-27','2017-12-01'),(5,1,2,'2017-11-28','2017-12-01'),(6,1,1,'2017-11-28','2017-12-05'),(7,3,1,'2017-11-28','2017-11-29'),(8,4,1,'2017-11-28','2017-11-29');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_has_furniture`
--

DROP TABLE IF EXISTS `transaction_has_furniture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_has_furniture` (
  `Transaction_TransactionID` int(11) NOT NULL,
  `Furniture_ItemNumber` int(11) NOT NULL,
  PRIMARY KEY (`Transaction_TransactionID`,`Furniture_ItemNumber`),
  KEY `fk_Transaction_has_Furniture_Furniture1_idx` (`Furniture_ItemNumber`),
  KEY `fk_Transaction_has_Furniture_Transaction1_idx` (`Transaction_TransactionID`),
  CONSTRAINT `fk_Transaction_has_Furniture_Furniture1` FOREIGN KEY (`Furniture_ItemNumber`) REFERENCES `furniture` (`ItemNumber`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transaction_has_Furniture_Transaction1` FOREIGN KEY (`Transaction_TransactionID`) REFERENCES `transaction` (`TransactionID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_has_furniture`
--

LOCK TABLES `transaction_has_furniture` WRITE;
/*!40000 ALTER TABLE `transaction_has_furniture` DISABLE KEYS */;
INSERT INTO `transaction_has_furniture` VALUES (1,1),(2,1),(5,1),(6,1),(7,1),(1,2),(2,2),(6,2),(8,2),(1,3),(2,3),(5,3),(3,4),(5,4),(3,5),(4,6),(4,7),(4,8),(3,11);
/*!40000 ALTER TABLE `transaction_has_furniture` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-28 21:41:12
