-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Style`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Style` (
  `StyleID` INT NOT NULL AUTO_INCREMENT,
  `StyleDescription` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`StyleID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Category` (
  `CategoryID` INT NOT NULL AUTO_INCREMENT,
  `CategoryDescription` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`CategoryID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Furniture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Furniture` (
  `ItemNumber` INT NOT NULL,
  `Style_StyleID` INT NOT NULL,
  `Category_CategoryID` INT NOT NULL,
  `Rate` DECIMAL(10,2) NULL,
  `SKU` VARCHAR(8) NULL,
  PRIMARY KEY (`ItemNumber`),
  UNIQUE INDEX `ItemNumber_UNIQUE` (`ItemNumber` ASC),
  INDEX `fk_Inventory_Style1_idx` (`Style_StyleID` ASC),
  INDEX `fk_Inventory_Category1_idx` (`Category_CategoryID` ASC),
  CONSTRAINT `fk_Inventory_Style1`
    FOREIGN KEY (`Style_StyleID`)
    REFERENCES `mydb`.`Style` (`StyleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Inventory_Category1`
    FOREIGN KEY (`Category_CategoryID`)
    REFERENCES `mydb`.`Category` (`CategoryID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Person` (
  `PersonID` INT NOT NULL AUTO_INCREMENT,
  `FName` VARCHAR(45) NOT NULL,
  `LName` VARCHAR(45) NOT NULL,
  `SSN` VARCHAR(45) NOT NULL,
  `Gender` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`PersonID`),
  UNIQUE INDEX `SSN_UNIQUE` (`SSN` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Member`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Member` (
  `MemberID` INT NOT NULL AUTO_INCREMENT,
  `Street` VARCHAR(45) NOT NULL,
  `City` VARCHAR(45) NOT NULL,
  `State` VARCHAR(45) NOT NULL,
  `Zip` VARCHAR(45) NOT NULL,
  `Phone` VARCHAR(45) NOT NULL,
  `Person_PersonID` INT NOT NULL,
  PRIMARY KEY (`MemberID`),
  INDEX `fk_Member_Person1_idx` (`Person_PersonID` ASC),
  CONSTRAINT `fk_Member_Person1`
    FOREIGN KEY (`Person_PersonID`)
    REFERENCES `mydb`.`Person` (`PersonID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Employee` (
  `EmployeeID` INT NOT NULL AUTO_INCREMENT,
  `IsAdministrator` TINYINT NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  `Person_PersonID` INT NOT NULL,
  PRIMARY KEY (`EmployeeID`),
  INDEX `fk_Employee_Person1_idx` (`Person_PersonID` ASC),
  CONSTRAINT `fk_Employee_Person1`
    FOREIGN KEY (`Person_PersonID`)
    REFERENCES `mydb`.`Person` (`PersonID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Transaction` (
  `TransactionID` INT NOT NULL AUTO_INCREMENT,
  `Member_MemberID` INT NOT NULL,
  `Employee_EmployeeID` INT NOT NULL,
  `TransactionDate` DATE NOT NULL,
  `DueDate` DATE NOT NULL,
  PRIMARY KEY (`TransactionID`),
  INDEX `fk_Transaction_Member1_idx` (`Member_MemberID` ASC),
  INDEX `fk_Transaction_Employee1_idx` (`Employee_EmployeeID` ASC),
  CONSTRAINT `fk_Transaction_Member1`
    FOREIGN KEY (`Member_MemberID`)
    REFERENCES `mydb`.`Member` (`MemberID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transaction_Employee1`
    FOREIGN KEY (`Employee_EmployeeID`)
    REFERENCES `mydb`.`Employee` (`EmployeeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Transaction_has_Furniture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Transaction_has_Furniture` (
  `Transaction_TransactionID` INT NOT NULL,
  `Furniture_ItemNumber` INT NOT NULL,
  PRIMARY KEY (`Transaction_TransactionID`, `Furniture_ItemNumber`),
  INDEX `fk_Transaction_has_Furniture_Furniture1_idx` (`Furniture_ItemNumber` ASC),
  INDEX `fk_Transaction_has_Furniture_Transaction1_idx` (`Transaction_TransactionID` ASC),
  CONSTRAINT `fk_Transaction_has_Furniture_Transaction1`
    FOREIGN KEY (`Transaction_TransactionID`)
    REFERENCES `mydb`.`Transaction` (`TransactionID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transaction_has_Furniture_Furniture1`
    FOREIGN KEY (`Furniture_ItemNumber`)
    REFERENCES `mydb`.`Furniture` (`ItemNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Return`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Return` (
  `Transaction_has_Furniture_Transaction_TransactionID` INT NOT NULL,
  `Transaction_has_Furniture_Furniture_ItemNumber` INT NOT NULL,
  `Employee_EmployeeID` INT NOT NULL,
  `ReturnDate` DATE NOT NULL,
  PRIMARY KEY (`Transaction_has_Furniture_Transaction_TransactionID`, `Transaction_has_Furniture_Furniture_ItemNumber`),
  INDEX `fk_Return_Employee1_idx` (`Employee_EmployeeID` ASC),
  CONSTRAINT `fk_Return_Transaction_has_Furniture1`
    FOREIGN KEY (`Transaction_has_Furniture_Transaction_TransactionID` , `Transaction_has_Furniture_Furniture_ItemNumber`)
    REFERENCES `mydb`.`Transaction_has_Furniture` (`Transaction_TransactionID` , `Furniture_ItemNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Return_Employee1`
    FOREIGN KEY (`Employee_EmployeeID`)
    REFERENCES `mydb`.`Employee` (`EmployeeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
