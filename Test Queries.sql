SELECT 
    CONCAT(renterPerson.FName,
            ' ',
            renterPerson.LName) AS renterName,
    Style.StyleDescription,
    Category.CategoryDescription,
    `transaction`.TransactionID,
    transaction_has_furniture.Furniture_ItemNumber AS ItemNumber,
    `transaction`.TransactionDate,
    `transaction`.DueDate,
    `return`.ReturnDate,
    CONCAT(rentalPerson.FName,
            ' ',
            rentalPerson.LName) AS rentPerson,
    CONCAT(returnPerson.FName,
            ' ',
            returnPerson.LName) AS retPerson
FROM
    transaction_has_furniture
        JOIN
    `transaction` ON `transaction`.TransactionID = transaction_has_furniture.Transaction_TransactionID
        JOIN
    member AS renter ON renter.MemberID = `transaction`.Member_MemberID
        JOIN
    person AS renterPerson ON renterPerson.PersonID = renter.Person_PersonID
        JOIN
    furniture ON furniture.ItemNumber = transaction_has_furniture.Furniture_ItemNumber
        JOIN
    Style ON Style.StyleID = furniture.Style_StyleID
        JOIN
    Category ON Category.CategoryID = furniture.Category_CategoryID
        LEFT JOIN
    `return` ON `return`.Transaction_has_Furniture_Furniture_ItemNumber = transaction_has_furniture.Furniture_ItemNumber
        AND `return`.Transaction_has_Furniture_Transaction_TransactionID = transaction_has_furniture.Transaction_TransactionID
        JOIN
    employee AS rentalEmployee ON rentalEmployee.EmployeeID = `transaction`.Employee_EmployeeID
        JOIN
    person AS rentalPerson ON rentalPerson.PersonID = rentalEmployee.Person_PersonID
        LEFT JOIN
    employee AS returnEmployee ON returnEmployee.EmployeeID = `return`.Employee_EmployeeID
        LEFT JOIN
    person AS returnPerson ON returnPerson.PersonID = returnEmployee.EmployeeID
WHERE
    renterPerson.FName = 'Third'
        AND renterPerson.LName = 'Last'
;

SELECT 
    MemberID,
    CONCAT(FName, ' ', LName) AS renterName,
    TransactionDate,
    DueDate,
    (SELECT 
            COUNT(Furniture_ItemNumber)
        FROM
            transaction_has_furniture
                JOIN
            `transaction` ON `transaction`.TransactionID = transaction_has_furniture.Transaction_TransactionID
        WHERE
            `transaction`.Member_MemberID = mem.MemberID
                AND `transaction`.TransactionID = tran.TransactionID) AS checkedOut
FROM
    member AS mem
        JOIN
    person ON PersonID = Person_PersonID
        JOIN
    `transaction` AS tran ON Member_MemberID = MemberID
WHERE
    (SELECT 
            COUNT(Furniture_ItemNumber)
        FROM
            transaction_has_furniture
                JOIN
            `transaction` ON `transaction`.TransactionID = transaction_has_furniture.Transaction_TransactionID
        WHERE
            `transaction`.Member_MemberID = mem.MemberID
                AND `transaction`.TransactionID = tran.TransactionID) >= 2
GROUP BY MemberID , TransactionID , DueDate
;

    
