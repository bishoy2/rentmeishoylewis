﻿using System;

namespace RentMeSystem.Model
{
    public class Transaction
    {
        public Transaction()
        {
            this.Member = new Member();
            this.Employee = new Employee();
        }

        [Column("TransactionId")]
        public int TransactionId { get; set; }

        public Member Member { get; set; }

        public Employee Employee { get; set; }

        [Column("TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [Column("DueDate")]
        public DateTime DueDate { get; set; }
    }
}