﻿using System;
using System.Data.Common;
using System.Linq;

namespace RentMeSystem.Model
{
    public class Column : Attribute
    {
        public Column(string columnName)
        {
            this.ColumnName = columnName;
        }

        public string ColumnName { get; }

        public static void WriteColumnMappings<T>(T item, DbDataRecord record) where T : new()
        {
            var type = item.GetType();

            var properties = item.GetType().GetProperties();

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(false);
                var columnMapping = attributes.FirstOrDefault(a => a.GetType() == typeof(Column));

                if (columnMapping == null) continue;

                var mapsto = columnMapping as Column;
                property.SetValue(item, record[mapsto.ColumnName]);
            }
        }
    }
}