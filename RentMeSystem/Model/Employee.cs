﻿namespace RentMeSystem.Model
{
    public class Employee
    {
        public Employee()
        {
            this.Person = new Person();
        }

        [Column("EmployeeID")]
        public int EmployeeId { get; set; }

        [Column("IsAdministrator")]
        public int IsAdministrator { get; set; }

        [Column("Password")]
        public string Password { get; set; }

        public Person Person { get; set; }
    }
}