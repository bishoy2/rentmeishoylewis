﻿using System;

namespace RentMeSystem.Model
{
    public class Member : IEquatable<Member>
    {
        public readonly Person Person;

        public Member()
        {
            this.Person = new Person();
        }

        [Column("MemberID")]
        public int MemberId { get; set; }

        [Column("Street")]
        public string Street { get; set; }

        [Column("City")]
        public string City { get; set; }

        [Column("State")]
        public string State { get; set; }

        [Column("Zip")]
        public string Zip { get; set; }

        [Column("Phone")]
        public string Phone { get; set; }

        public string FirstName => this.Person.FirstName;

        public string LastName => this.Person.LastName;

        public string Ssn => this.Person.Ssn;

        public bool Equals(Member other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.MemberId == other.MemberId;
        }

        public override string ToString()
        {
            return $"{this.MemberId}: {this.Person.FirstName} {this.Person.LastName}";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((Member) obj);
        }

        public override int GetHashCode()
        {
            return this.MemberId;
        }
    }
}