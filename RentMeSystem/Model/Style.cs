﻿namespace RentMeSystem.Model
{
    public class Style
    {
        [Column("StyleID")]
        public int StyleId { get; set; }

        [Column("StyleDescription")]
        public string StyleDescription { get; set; }

        public override string ToString()
        {
            return this.StyleDescription;
        }
    }
}