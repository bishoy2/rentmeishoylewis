﻿namespace RentMeSystem.Model
{
    public class Category
    {
        [Column("CategoryID")]
        public int CategoryId { get; set; }

        [Column("CategoryDescription")]
        public string CategoryDescription { get; set; }

        public override string ToString()
        {
            return this.CategoryDescription;
        }
    }
}