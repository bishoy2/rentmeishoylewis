﻿using System;

namespace RentMeSystem.Model
{
    public class Return
    {
        public TransactionHasFurniture Furniture { get; set; }

        public Employee Employee { get; set; }

        [Column("ReturnDate")]
        public DateTime ReturnDate { get; set; }
    }
}