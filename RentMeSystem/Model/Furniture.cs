﻿using System;

namespace RentMeSystem.Model
{
    public class Furniture : IEquatable<Furniture>
    {
        public Furniture()
        {
            this.Style = new Style();
            this.Category = new Category();
        }

        [Column("ItemNumber")]
        public int ItemNumber { get; set; }

        public Style Style { get; set; }

        public Category Category { get; set; }

        [Column("Rate")]
        public decimal Rate { get; set; }

        [Column("SKU")]
        public string Sku { get; set; }

        public bool Equals(Furniture other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.ItemNumber == other.ItemNumber;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((Furniture) obj);
        }

        public override int GetHashCode()
        {
            return this.ItemNumber;
        }
    }
}