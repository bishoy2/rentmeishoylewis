﻿namespace RentMeSystem.Model
{
    public class Person
    {
        [Column("PersonID")]
        public int PersonId { get; set; }

        [Column("FName")]
        public string FirstName { get; set; }

        [Column("LName")]
        public string LastName { get; set; }

        [Column("SSN")]
        public string Ssn { get; set; }

        [Column("Gender")]
        public string Gender { get; set; }
    }
}