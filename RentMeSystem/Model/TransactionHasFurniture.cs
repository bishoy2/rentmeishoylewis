﻿using System;

namespace RentMeSystem.Model
{
    public class TransactionHasFurniture : IEquatable<TransactionHasFurniture>
    {
        public readonly Furniture Furniture;

        public readonly Transaction Transaction;

        public TransactionHasFurniture()
        {
            this.Transaction = new Transaction();
            this.Furniture = new Furniture();
        }

        public int TransactionId => this.Transaction.TransactionId;

        public DateTime TransactionDate => this.Transaction.TransactionDate;

        public int ItemNumber => this.Furniture.ItemNumber;

        public Style Style => this.Furniture.Style;

        public Category Category => this.Furniture.Category;

        public int MemberId => this.Transaction.Member.MemberId;

        public string LastName => this.Transaction.Member.Person.LastName;

        public bool Equals(TransactionHasFurniture other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(this.TransactionId, other.TransactionId) && Equals(this.ItemNumber, other.ItemNumber);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((TransactionHasFurniture) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((this.Furniture != null ? this.Furniture.GetHashCode() : 0) * 397) ^
                       (this.Transaction != null ? this.Transaction.GetHashCode() : 0);
            }
        }
    }
}