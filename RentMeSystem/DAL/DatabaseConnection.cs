﻿using System.Configuration;
using MySql.Data.MySqlClient;

namespace RentMeSystem.DAL
{
    public class DatabaseConnection
    {
        public static MySqlConnection GetConnection(string connectionLabel)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionLabel].ConnectionString;
            var connection = new MySqlConnection(connectionString);
            return connection;
        }
    }
}