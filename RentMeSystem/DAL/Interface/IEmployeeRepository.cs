﻿using RentMeSystem.Model;

namespace RentMeSystem.DAL.Interface
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        bool Authenticate(string employeeId, string password);
    }
}