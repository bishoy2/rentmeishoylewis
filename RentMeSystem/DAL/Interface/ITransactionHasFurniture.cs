﻿using System.Collections.Generic;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Interface
{
    public interface ITransactionHasFurniture : IRepository<TransactionHasFurniture>
    {
        void Rent(Transaction transaction, IEnumerable<Furniture> Furniture);
        void Return(IEnumerable<Return> returns);
        IEnumerable<TransactionHasFurniture> GetByMemberId(int memberId);
    }
}