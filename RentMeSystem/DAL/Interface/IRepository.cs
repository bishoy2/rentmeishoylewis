﻿using System.Collections.Generic;

namespace RentMeSystem.DAL.Interface
{
    public interface IRepository<T>
    {
        void Add(T entity);
        T GetById(int id);
        IEnumerable<T> GetAll();
    }
}