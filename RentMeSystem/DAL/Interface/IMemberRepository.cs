﻿using System.Collections.Generic;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Interface
{
    public interface IMemberRepository : IRepository<Member>
    {
        void Update(Member Member);
        IEnumerable<Member> GetByFirst(string firstName);
        IEnumerable<Member> GetByLast(string lastName);
        IEnumerable<Member> GetByName(string name);
        IEnumerable<Member> GetByGender(string gender);
        IEnumerable<Member> GetByMemberId(string memberId);
        IEnumerable<Member> GetByMemberSsn(string ssn);
        IEnumerable<Member> GetByStreet(string street);
        IEnumerable<Member> GetByCity(string city);
        IEnumerable<Member> GetByState(string state);
        IEnumerable<Member> GetByZip(string zip);
        IEnumerable<Member> GetByPhoneNumber(string phoneNumber);
    }
}