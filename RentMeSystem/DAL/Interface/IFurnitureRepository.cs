﻿using System.Collections.Generic;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Interface
{
    public interface IFurnitureRepository : IRepository<Furniture>
    {
        IEnumerable<Furniture> GetByCategory(string category);
        IEnumerable<Furniture> GetByStyle(string style);
        IEnumerable<Furniture> GetByItemNumber(string itemNumber);
        IEnumerable<Furniture> GetBySku(string sku);
    }
}