﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using MySql.Data.MySqlClient;
using RentMeSystem.DAL.Interface;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Repository
{
    public class MemberRepository : IMemberRepository
    {
        private readonly string _connectionLabel;

        public MemberRepository(string connectionLabel = "MySqlDatabaseConnection")
        {
            this._connectionLabel = connectionLabel;
        }

        public void Add(Member entity)
        {
            using (var conn = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                conn.Open();

                using (var command = conn.CreateCommand())
                {
                    command.Transaction = conn.BeginTransaction();

                    command.CommandText =
                        "INSERT INTO Person (FName, LName, SSN, Gender) Values (@firstName, @lastName, @ssn, @gender);" +
                        "INSERT INTO Member (Person_PersonID, Street, City, State, Zip, Phone) VALUES ((SELECT LAST_INSERT_ID()), @street, @city, @state, @zip, @phone);";

                    command.Parameters.AddWithValue("@firstName", entity.Person.FirstName);
                    command.Parameters.AddWithValue("@lastName", entity.Person.LastName);
                    command.Parameters.AddWithValue("@ssn", entity.Person.Ssn);
                    command.Parameters.AddWithValue("@gender", entity.Person.Gender);

                    command.Parameters.AddWithValue("@street", entity.Street);
                    command.Parameters.AddWithValue("@city", entity.City);
                    command.Parameters.AddWithValue("@state", entity.State);
                    command.Parameters.AddWithValue("@zip", entity.Zip);
                    command.Parameters.AddWithValue("@phone", entity.Phone);

                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                }

                conn.Close();
            }
        }

        public Member GetById(int id)
        {
            return this.GetAll().FirstOrDefault(Member => Member.MemberId == id);
        }

        public IEnumerable<Member> GetAll()
        {
            using (var connection = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                connection.Open();

                using (var command =
                    new MySqlCommand("SELECT * FROM Member JOIN Person ON Person.PersonID = Member.Person_PersonID",
                        connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        foreach (DbDataRecord record in reader)
                        {
                            var Member = new Member();
                            Column.WriteColumnMappings(Member, record);
                            Column.WriteColumnMappings(Member.Person, record);
                            yield return Member;
                        }
                    }
                }


                connection.Close();
            }
        }

        public void Update(Member Member)
        {
            using (var conn = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                conn.Open();

                using (var command = conn.CreateCommand())
                {
                    command.Transaction = conn.BeginTransaction();

                    command.CommandText =
                        "UPDATE Person SET FName = @firstName, LName = @lastName, SSN = @ssn, Gender = @gender Where PersonID = @personID;" +
                        "UPDATE Member SET Street = @street, City = @city, State = @state, Zip = @zip, Phone = @phone Where Person_PersonID = @personID;";

                    command.Parameters.AddWithValue("@firstName", Member.Person.FirstName);
                    command.Parameters.AddWithValue("@lastName", Member.Person.LastName);
                    command.Parameters.AddWithValue("@ssn", Member.Person.Ssn);
                    command.Parameters.AddWithValue("@gender", Member.Person.Gender);
                    command.Parameters.AddWithValue("@personID", Member.Person.PersonId);


                    command.Parameters.AddWithValue("@street", Member.Street);
                    command.Parameters.AddWithValue("@city", Member.City);
                    command.Parameters.AddWithValue("@state", Member.State);
                    command.Parameters.AddWithValue("@zip", Member.Zip);
                    command.Parameters.AddWithValue("@phone", Member.Phone);

                    command.ExecuteNonQuery();

                    command.Transaction.Commit();
                }

                conn.Close();
            }
        }

        public IEnumerable<Member> GetByFirst(string firstName)
        {
            return this.GetAll().Where(Member => Member.FirstName.ToLower().Contains(firstName.ToLower()));
        }

        public IEnumerable<Member> GetByLast(string lastName)
        {
            return this.GetAll().Where(Member => Member.LastName.ToLower().Contains(lastName.ToLower()));
        }

        public IEnumerable<Member> GetByMemberId(string memberId)
        {
            return this.GetAll().Where(Member => Member.MemberId.ToString().Equals(memberId));
        }

        public IEnumerable<Member> GetByMemberSsn(string ssn)
        {
            return this.GetAll().Where(Member => Member.Ssn == ssn);
        }

        public IEnumerable<Member> GetByStreet(string street)
        {
            return this.GetAll().Where(Member => Member.Street.ToLower().Contains(street.ToLower()));
        }

        public IEnumerable<Member> GetByCity(string city)
        {
            return this.GetAll().Where(Member => Member.City.ToLower().Contains(city.ToLower()));
        }

        public IEnumerable<Member> GetByState(string state)
        {
            return this.GetAll().Where(Member => Member.State.ToLower().Contains(state.ToLower()));
        }

        public IEnumerable<Member> GetByZip(string zip)
        {
            return this.GetAll().Where(Member => Member.Zip.Contains(zip));
        }

        public IEnumerable<Member> GetByPhoneNumber(string phoneNumber)
        {
            return this.GetAll().Where(Member => Member.Phone.Contains(phoneNumber));
        }

        public IEnumerable<Member> GetByGender(string gender)
        {
            return this.GetAll().Where(Member => Member.Person.Gender.Contains(gender));
        }

        public IEnumerable<Member> GetByName(string name)
        {
            return this.GetAll().Where((Member, i) =>
            {
                var memberName = Member.FirstName + " " + Member.LastName;
                return memberName.ToLower().Contains(name.ToLower());
            });
        }
    }
}