﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using MySql.Data.MySqlClient;
using RentMeSystem.DAL.Interface;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly string _connectionLabel;

        public EmployeeRepository(string connectionLabel = "MySqlDatabaseConnection")
        {
            this._connectionLabel = connectionLabel;
        }

        public void Add(Employee entity)
        {
            throw new NotImplementedException();
        }

        public Employee GetById(int id)
        {
            return this.GetAll().FirstOrDefault(employee => employee.EmployeeId == id);
        }

        public IEnumerable<Employee> GetAll()
        {
            using (var connection = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                connection.Open();

                using (var command =
                    new MySqlCommand("SELECT * FROM Employee JOIN Person ON Person.PersonID = Employee.Person_PersonID",
                        connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        foreach (DbDataRecord record in reader)
                        {
                            var employee = new Employee();
                            Column.WriteColumnMappings(employee, record);
                            Column.WriteColumnMappings(employee.Person, record);
                            yield return employee;
                        }
                    }
                }


                connection.Close();
            }
        }

        public bool Authenticate(string employeeId, string password)
        {
            object result = null;

            using (var conn = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    command.CommandText =
                        "SELECT EmployeeID FROM Employee WHERE EmployeeID = @empId AND Password = PASSWORD(@pass)";
                    command.Parameters.AddWithValue("@empId", employeeId);
                    command.Parameters.AddWithValue("@pass", password);

                    result = command.ExecuteScalar();
                }
                conn.Close();
            }

            return result != null;
        }
    }
}