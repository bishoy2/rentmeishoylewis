﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using MySql.Data.MySqlClient;
using RentMeSystem.DAL.Interface;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Repository
{
    public class FurnitureRepository : IFurnitureRepository
    {
        private readonly string _connectionLabel;

        public FurnitureRepository(string connectionLabel = "MySqlDatabaseConnection")
        {
            this._connectionLabel = connectionLabel;
        }


        public void Add(Furniture entity)
        {
            throw new NotImplementedException();
        }

        public Furniture GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Furniture> GetAll()
        {
            using (var connection = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                connection.Open();

                using (var command =
                    new MySqlCommand(
                        "SELECT * FROM Furniture JOIN Style ON Style.StyleID = Furniture.Style_StyleID JOIN Category ON Category.CategoryID = Furniture.Category_CategoryID WHERE Furniture.ItemNumber NOT IN (SELECT Furniture_ItemNumber FROM Transaction_has_Furniture LEFT JOIN `Return` ON `Return`.Transaction_has_Furniture_Furniture_ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber AND `Return`.Transaction_has_Furniture_Transaction_TransactionID = Transaction_has_Furniture.Transaction_TransactionID WHERE `Return`.Transaction_has_Furniture_Furniture_ItemNumber IS NULL);",
                        connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        foreach (DbDataRecord record in reader)
                        {
                            var Furniture = new Furniture();
                            Column.WriteColumnMappings(Furniture, record);
                            Column.WriteColumnMappings(Furniture.Category, record);
                            Column.WriteColumnMappings(Furniture.Style, record);

                            yield return Furniture;
                        }
                    }
                }


                connection.Close();
            }
        }

        public IEnumerable<Furniture> GetByCategory(string category)
        {
            return this.GetAll().Where(Furniture => Furniture.Category.CategoryDescription.Contains(category));
        }

        public IEnumerable<Furniture> GetByStyle(string style)
        {
            return this.GetAll().Where(Furniture => Furniture.Style.StyleDescription.Contains(style));
        }

        public IEnumerable<Furniture> GetByItemNumber(string itemNumber)
        {
            return this.GetAll().Where(furn => furn.ItemNumber.ToString().Equals(itemNumber)).ToList();
        }

        public IEnumerable<Furniture> GetBySku(string sku)
        {
            return this.GetAll().Where(Furniture => Furniture.Sku.Contains(sku));
        }
    }
}