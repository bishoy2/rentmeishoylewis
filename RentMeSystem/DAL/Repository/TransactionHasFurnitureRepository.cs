﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using MySql.Data.MySqlClient;
using RentMeSystem.DAL.Interface;
using RentMeSystem.Model;

namespace RentMeSystem.DAL.Repository
{
    public class TransactionHasFurnitureRepository : ITransactionHasFurniture
    {
        private readonly string _connectionLabel;

        public TransactionHasFurnitureRepository(string connectionLabel = "MySqlDatabaseConnection")
        {
            this._connectionLabel = connectionLabel;
        }

        public void Add(TransactionHasFurniture entity)
        {
            throw new NotImplementedException();
        }

        public TransactionHasFurniture GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TransactionHasFurniture> GetAll()
        {
            using (var connection = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                connection.Open();

                using (var command =
                    new MySqlCommand(
                        "SELECT * FROM `Transaction` JOIN Transaction_has_Furniture ON Transaction_has_Furniture.Transaction_TransactionID = `Transaction`.TransactionID JOIN Member ON Member.MemberID = `Transaction`.Member_MemberID JOIN Furniture ON Furniture.ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber JOIN Style ON Style.StyleID = Furniture.Style_StyleID JOIN Category ON Category.CategoryID = Furniture.Category_CategoryID JOIN Person ON Person.PersonID = Member.Person_PersonID LEFT JOIN `Return` ON `Return`.Transaction_has_Furniture_Furniture_ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber AND `Return`.Transaction_has_Furniture_Transaction_TransactionID = Transaction_has_Furniture.Transaction_TransactionID WHERE `Return`.Transaction_has_Furniture_Furniture_ItemNumber IS NULL;",
                        connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        foreach (DbDataRecord record in reader)
                        {
                            var transactionHasFurniture = new TransactionHasFurniture();

                            Column.WriteColumnMappings(transactionHasFurniture.Transaction, record);
                            Column.WriteColumnMappings(transactionHasFurniture.Furniture, record);
                            Column.WriteColumnMappings(transactionHasFurniture.Furniture.Category, record);
                            Column.WriteColumnMappings(transactionHasFurniture.Furniture.Style, record);
                            Column.WriteColumnMappings(transactionHasFurniture.Transaction.Member, record);
                            Column.WriteColumnMappings(transactionHasFurniture.Transaction.Member.Person, record);

                            yield return transactionHasFurniture;
                        }
                    }
                }


                connection.Close();
            }
        }

        public void Rent(Transaction transaction, IEnumerable<Furniture> Furniture)
        {
            using (var conn = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                conn.Open();

                using (var command = conn.CreateCommand())
                {
                    command.Transaction = conn.BeginTransaction();

                    command.CommandText =
                        "INSERT INTO Transaction (Member_MemberID, Employee_EmployeeID, TransactionDate, DueDate) Values (@memberId, @employeeId, @transactionDate, @dueDate)";

                    command.Parameters.AddWithValue("@memberId", transaction.Member.MemberId);
                    command.Parameters.AddWithValue("@employeeId", transaction.Employee.EmployeeId);
                    command.Parameters.AddWithValue("@transactionDate", transaction.TransactionDate);
                    command.Parameters.AddWithValue("@dueDate", transaction.DueDate);

                    command.ExecuteNonQuery();

                    var transactionId = command.LastInsertedId;

                    command.CommandText = "INSERT INTO Transaction_has_Furniture VALUES (@transactionId,@itemNumber)";
                    command.Prepare();

                    command.Parameters.AddWithValue("@transactionId", null);
                    command.Parameters.AddWithValue("@itemNumber", null);

                    foreach (var inventory in Furniture)
                    {
                        command.Parameters["@transactionId"].Value = transactionId;
                        command.Parameters["@itemNumber"].Value = inventory.ItemNumber;

                        command.ExecuteNonQuery();
                    }


                    command.Transaction.Commit();
                }

                conn.Close();
            }
        }

        public void Return(IEnumerable<Return> returns)
        {
            using (var conn = DatabaseConnection.GetConnection(this._connectionLabel))
            {
                conn.Open();

                using (var command = conn.CreateCommand())
                {
                    command.Transaction = conn.BeginTransaction();

                    command.CommandText =
                        "INSERT INTO `Return` VALUES (@transactionId,@itemNumber,@employeeId,@returnDate)";
                    command.Prepare();

                    command.Parameters.AddWithValue("@transactionId", null);
                    command.Parameters.AddWithValue("@itemNumber", null);
                    command.Parameters.AddWithValue("@employeeId", null);
                    command.Parameters.AddWithValue("@returnDate", null);

                    foreach (var @return in returns)
                    {
                        command.Parameters["@transactionId"].Value = @return.Furniture.TransactionId;
                        command.Parameters["@itemNumber"].Value = @return.Furniture.ItemNumber;
                        command.Parameters["@employeeId"].Value = @return.Employee.EmployeeId;
                        command.Parameters["@returnDate"].Value = @return.ReturnDate;

                        command.ExecuteNonQuery();
                    }


                    command.Transaction.Commit();
                }

                conn.Close();
            }
        }

        public IEnumerable<TransactionHasFurniture> GetByMemberId(int memberId)
        {
            return this.GetAll().Where(Furniture => Furniture.MemberId == memberId);
        }
    }
}