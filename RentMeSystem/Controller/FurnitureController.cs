﻿using System.Collections.Generic;
using RentMeSystem.DAL.Interface;
using RentMeSystem.DAL.Repository;
using RentMeSystem.Model;

namespace RentMeSystem.Controller
{
    internal class FurnitureController : IFurnitureRepository
    {
        private readonly IFurnitureRepository _repo;

        public FurnitureController()
        {
            this._repo = new FurnitureRepository();
        }

        public FurnitureController(IFurnitureRepository repo)
        {
            this._repo = repo;
        }


        public void Add(Furniture entity)
        {
            this._repo.Add(entity);
        }

        public Furniture GetById(int id)
        {
            return this._repo.GetById(id);
        }

        public IEnumerable<Furniture> GetAll()
        {
            return this._repo.GetAll();
        }

        public IEnumerable<Furniture> GetByCategory(string category)
        {
            return this._repo.GetByCategory(category);
        }

        public IEnumerable<Furniture> GetByStyle(string style)
        {
            return this._repo.GetByStyle(style);
        }

        public IEnumerable<Furniture> GetByItemNumber(string itemNumber)
        {
            return this._repo.GetByItemNumber(itemNumber);
        }

        public IEnumerable<Furniture> GetBySku(string sku)
        {
            return this._repo.GetBySku(sku);
        }
    }
}