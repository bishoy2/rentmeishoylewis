﻿using System.Collections.Generic;
using RentMeSystem.DAL.Interface;
using RentMeSystem.DAL.Repository;
using RentMeSystem.Model;

namespace RentMeSystem.Controller
{
    public class MemberController : IMemberRepository
    {
        private readonly IMemberRepository _repo;

        public MemberController()
        {
            this._repo = new MemberRepository();
        }

        public MemberController(IMemberRepository repo)
        {
            this._repo = repo;
        }


        public void Add(Member entity)
        {
            this._repo.Add(entity);
        }

        public Member GetById(int id)
        {
            return this._repo.GetById(id);
        }

        public IEnumerable<Member> GetAll()
        {
            return this._repo.GetAll();
        }

        public void Update(Member Member)
        {
            this._repo.Update(Member);
        }

        public IEnumerable<Member> GetByFirst(string firstName)
        {
            return this._repo.GetByFirst(firstName);
        }

        public IEnumerable<Member> GetByLast(string lastName)
        {
            return this._repo.GetByLast(lastName);
        }

        public IEnumerable<Member> GetByName(string name)
        {
            return this._repo.GetByName(name);
        }

        public IEnumerable<Member> GetByGender(string gender)
        {
            return this._repo.GetByGender(gender);
        }

        public IEnumerable<Member> GetByMemberId(string memberId)
        {
            return this._repo.GetByMemberId(memberId);
        }

        public IEnumerable<Member> GetByMemberSsn(string ssn)
        {
            return this._repo.GetByMemberSsn(ssn);
        }

        public IEnumerable<Member> GetByStreet(string street)
        {
            return this._repo.GetByStreet(street);
        }

        public IEnumerable<Member> GetByCity(string city)
        {
            return this._repo.GetByCity(city);
        }

        public IEnumerable<Member> GetByState(string state)
        {
            return this._repo.GetByState(state);
        }

        public IEnumerable<Member> GetByZip(string zip)
        {
            return this._repo.GetByZip(zip);
        }

        public IEnumerable<Member> GetByPhoneNumber(string phoneNumber)
        {
            return this._repo.GetByPhoneNumber(phoneNumber);
        }
    }
}