﻿using System;
using System.Collections.Generic;
using RentMeSystem.DAL.Interface;
using RentMeSystem.DAL.Repository;
using RentMeSystem.Model;

namespace RentMeSystem.Controller
{
    public class TransactionHasFurnitureController : ITransactionHasFurniture
    {
        private readonly ITransactionHasFurniture _repo;

        public TransactionHasFurnitureController()
        {
            this._repo = new TransactionHasFurnitureRepository();
        }

        public TransactionHasFurnitureController(ITransactionHasFurniture repo)
        {
            this._repo = repo;
        }

        public void Add(TransactionHasFurniture entity)
        {
            this._repo.Add(entity);
        }

        public TransactionHasFurniture GetById(int id)
        {
            return this._repo.GetById(id);
        }

        public IEnumerable<TransactionHasFurniture> GetAll()
        {
            return this._repo.GetAll();
        }

        public void Rent(Transaction transaction, IEnumerable<Furniture> Furniture)
        {
            this._repo.Rent(transaction, Furniture);
        }

        public void Return(IEnumerable<Return> returns)
        {
            this._repo.Return(returns);
        }

        public IEnumerable<TransactionHasFurniture> GetByMemberId(int memberId)
        {
            return this._repo.GetByMemberId(memberId);
        }

        internal object GetAll(string text)
        {
            throw new NotImplementedException();
        }
    }
}