﻿using System.Collections.Generic;
using RentMeSystem.DAL.Interface;
using RentMeSystem.DAL.Repository;
using RentMeSystem.Model;

namespace RentMeSystem.Controller
{
    public class EmployeeController : IEmployeeRepository
    {
        private readonly IEmployeeRepository _repo;

        public EmployeeController()
        {
            this._repo = new EmployeeRepository();
        }

        public EmployeeController(IEmployeeRepository repo)
        {
            this._repo = repo;
        }

        public void Add(Employee entity)
        {
            this._repo.Add(entity);
        }

        public bool Authenticate(string employeeId, string password)
        {
            return this._repo.Authenticate(employeeId, password);
        }

        public IEnumerable<Employee> GetAll()
        {
            return this._repo.GetAll();
        }

        public Employee GetById(int id)
        {
            return this._repo.GetById(id);
        }
    }
}