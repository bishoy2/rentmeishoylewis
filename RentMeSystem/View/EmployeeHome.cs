﻿using System;
using System.Windows.Forms;
using RentMeSystem.Model;

namespace RentMeSystem.View
{
    public partial class EmployeeHome : Form
    {
        private readonly Employee _currentEmployee;

        public EmployeeHome(Employee employee)
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;

            this._currentEmployee = employee;
            this.usernameLabel.Text =
                $"Welcome, {this._currentEmployee.Person.FirstName} {this._currentEmployee.Person.LastName}";

            if (this._currentEmployee.IsAdministrator == 0)
                this.queryInterfaceButton.Enabled = false;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            using (var registration = new MemberRegistration())
            {
                registration.ShowDialog();
            }
        }

        private void viewEditMemberButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var membersView = new MembersView())
            {
                membersView.ShowDialog();
            }
            this.Show();
        }

        private void searchFurnitureButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var furnitureView = new FurnitureView(this._currentEmployee))
            {
                furnitureView.ShowDialog();
            }
            this.Show();
        }

        private void viewRentalsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var rentalsView = new RentalsView(this._currentEmployee))
            {
                rentalsView.ShowDialog();
            }
            this.Show();
        }

        private void queryInterfaceButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var administratorView = new QueryView())
            {
                administratorView.ShowDialog();
            }
            this.Show();
        }

        private void transactionViewButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (var historyView = new HistoryView())
            {
                historyView.ShowDialog();
            }
            this.Show();
        }
    }
}