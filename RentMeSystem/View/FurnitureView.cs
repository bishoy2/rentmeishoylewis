﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using RentMeSystem.Controller;
using RentMeSystem.Model;

namespace RentMeSystem.View
{
    public partial class FurnitureView : Form
    {
        private readonly FurnitureController _furnitureController;
        private readonly MemberController _memberController;
        private readonly BindingList<Furniture> _selectedFurniture;
        private readonly TransactionHasFurnitureController _transactionHasFurnitureController;

        private readonly Employee currentEmployee;


        public FurnitureView(Employee employee)
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.rentDatePicker.MinDate = DateTime.Now.AddDays(1);

            this._furnitureController = new FurnitureController();
            this._memberController = new MemberController();
            this._selectedFurniture = new BindingList<Furniture>();
            this._transactionHasFurnitureController = new TransactionHasFurnitureController();
            this.currentEmployee = employee;

            this.searchBox.DataSource = Enum.GetValues(typeof(FurnitureSearchOptions));
            this.memberBox.DataSource = this._memberController.GetAll().ToList();

            this.UpdateSource();
        }

        private void UpdateSource()
        {
            this.stockGridView.DataSource = this._furnitureController.GetAll().ToList();
            this.selectedGridView.DataSource = this._selectedFurniture;

            float stockPreferredWidths = this.stockGridView.Columns.OfType<DataGridViewColumn>().Sum(column =>
                column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true));

            foreach (var column in this.stockGridView.Columns.OfType<DataGridViewColumn>())
            {
                var preferredWidth =
                    column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true);
                column.FillWeight = preferredWidth /
                                    stockPreferredWidths;
            }


            float selectedPreferredWidths = this.selectedGridView.Columns.OfType<DataGridViewColumn>().Sum(column =>
                column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true));

            foreach (var column in this.selectedGridView.Columns.OfType<DataGridViewColumn>())
            {
                var preferredWidth =
                    column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true);
                column.FillWeight = preferredWidth /
                                    selectedPreferredWidths;
            }
        }

        private void stockGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dataBoundItem = this.stockGridView.Rows[e.RowIndex].DataBoundItem as Furniture;
            if (!this._selectedFurniture.Contains(dataBoundItem))
                this._selectedFurniture.Add(dataBoundItem);
        }

        private void selectedGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dataBoundItem = this.selectedGridView.Rows[e.RowIndex].DataBoundItem as Furniture;
            this._selectedFurniture.Remove(dataBoundItem);
        }

        private void rentButton_Click(object sender, EventArgs e)
        {
            if (!this._selectedFurniture.Any())
            {
                MessageBox.Show("No Items Selected");
                return;
            }

            if (this.memberBox.SelectedItem == null)
            {
                MessageBox.Show("No Member Selected");
                return;
            }

            try
            {
                var confirmation = MessageBox.Show("Are you sure you want to rent this Furniture costing " +
                                                   (this._selectedFurniture.Sum(x => Convert.ToDouble(x.Rate)) *
                                                    Math.Round((this.rentDatePicker.Value - DateTime.Now).TotalDays))
                                                   .ToString("C") +
                                                   ", due on " + this.rentDatePicker.Value.ToShortDateString() + "?",
                    "Rental Confirmation", MessageBoxButtons.YesNo);

                if (confirmation == DialogResult.Yes)
                {
                    this._transactionHasFurnitureController.Rent(new Transaction
                    {
                        Member = this.memberBox.SelectedItem as Member,
                        Employee = this.currentEmployee,
                        TransactionDate = DateTime.Now,
                        DueDate = this.rentDatePicker.Value
                    }, this._selectedFurniture);

                    MessageBox.Show("Rental Successful!");

                    this._selectedFurniture.Clear();
                    this.UpdateSource();
                }
            }
            catch (MySqlException mse)
            {
                var unsuccessfulBox = MessageBox.Show("Rental Failed! Error info: " + mse.Message);
            }
        }

        private void searchField_TextChanged(object sender, EventArgs e)
        {
            if (this.searchField.Text == string.Empty)
            {
                this.stockGridView.DataSource = this._furnitureController.GetAll().ToList();
            }
            else
            {
                var searchOption = (FurnitureSearchOptions) Enum.GetValues(typeof(FurnitureSearchOptions))
                    .GetValue(this.searchBox.SelectedIndex);
                var searchText = this.searchField.Text;

                switch (searchOption)
                {
                    case FurnitureSearchOptions.ItemNumber:
                        this.stockGridView.DataSource = this._furnitureController.GetByItemNumber(searchText).ToList();
                        break;
                    case FurnitureSearchOptions.Category:
                        this.stockGridView.DataSource = this._furnitureController.GetByCategory(searchText).ToList();
                        break;
                    case FurnitureSearchOptions.Style:
                        this.stockGridView.DataSource = this._furnitureController.GetByStyle(searchText).ToList();
                        break;
                    case FurnitureSearchOptions.Sku:
                        this.stockGridView.DataSource = this._furnitureController.GetBySku(searchText).ToList();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void searchBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.searchField.Text = string.Empty;
        }

        private enum FurnitureSearchOptions
        {
            ItemNumber,
            Category,
            Style,
            Sku
        }
    }
}