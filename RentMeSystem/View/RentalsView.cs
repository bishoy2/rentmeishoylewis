using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using RentMeSystem.Controller;
using RentMeSystem.Model;

namespace RentMeSystem.View
{
    public partial class RentalsView : Form
    {
        private readonly Employee _currentEmployee;
        private readonly BindingList<TransactionHasFurniture> _selectedFurniture;
        private readonly TransactionHasFurnitureController _transactionHasFurnitureController;

        public RentalsView(Employee employee)
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;

            this._transactionHasFurnitureController = new TransactionHasFurnitureController();
            this._selectedFurniture = new BindingList<TransactionHasFurniture>();
            this._currentEmployee = employee;

            this.memberBox.DataSource = this._transactionHasFurnitureController.GetAll()
                .Select(Furniture => Furniture.Transaction.Member).Distinct().ToList();

            this.UpdateSource();
        }

        private void UpdateSource()
        {
            if (this.memberBox.SelectedItem == null)
            {
                this.rentalsGridView.DataSource = null;
                return;
            }

            var selectedMember = this.memberBox.SelectedItem as Member;

            this.rentalsGridView.DataSource = this._transactionHasFurnitureController
                .GetByMemberId(selectedMember.MemberId).ToList();

            this.selectedGridView.DataSource = this._selectedFurniture;

            this.rentalsGridView.Columns[1].DefaultCellStyle.Format = "MM/dd/yyyy";

            float preferredWidths = this.rentalsGridView.Columns.OfType<DataGridViewColumn>().Sum(column =>
                column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true));

            foreach (var column in this.rentalsGridView.Columns.OfType<DataGridViewColumn>())
            {
                var preferredWidth =
                    column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true);
                column.FillWeight = preferredWidth /
                                    preferredWidths;
            }
        }

        private void rentalsGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dataBoundItem = this.rentalsGridView.Rows[e.RowIndex].DataBoundItem as TransactionHasFurniture;
            if (!this._selectedFurniture.Contains(dataBoundItem))
                this._selectedFurniture.Add(dataBoundItem);
        }

        private void selectedGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dataBoundItem = this.selectedGridView.Rows[e.RowIndex].DataBoundItem as TransactionHasFurniture;
            this._selectedFurniture.Remove(dataBoundItem);
        }

        private void memberBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._selectedFurniture.Clear();
            this.UpdateSource();
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            if (!this._selectedFurniture.Any())
            {
                MessageBox.Show("No Items Marked For Return");
                return;
            }

            var sumFines = this._selectedFurniture.Sum(Furniture =>
                Convert.ToDouble(Furniture.Furniture.Rate) *
                Math.Round((DateTime.Now - Furniture.Transaction.DueDate).TotalDays));
            if (sumFines > 0)
                MessageBox.Show("You returned one or more pieces of Furniture late. You owe: $" + sumFines);

            var returns = this._selectedFurniture.Select(Furniture => new Return
            {
                Furniture = Furniture,
                Employee = this._currentEmployee,
                ReturnDate = DateTime.Now
            });

            this._transactionHasFurnitureController.Return(returns);
            this._selectedFurniture.Clear();
            this.memberBox.DataSource = this._transactionHasFurnitureController.GetAll()
                .Select(Furniture => Furniture.Transaction.Member).Distinct().ToList();

            if (this.memberBox.SelectedItem == null)
                this.UpdateSource();
        }
    }
}