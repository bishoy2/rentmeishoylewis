﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using RentMeSystem.DAL;

namespace RentMeSystem.View
{
    public partial class QueryView : Form
    {
        public QueryView()
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            using (var conn = DatabaseConnection.GetConnection("MySqlDatabaseConnection"))
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    if (this.userInput.Text == string.Empty)
                        return;
                    try
                    {
                        command.CommandText = this.userInput.Text;
                        var dataAdapter = new MySqlDataAdapter(command);
                        var dataTable = new DataTable();
                        dataAdapter.Fill(dataTable);
                        this.resultGridView.DataSource = dataTable;

                        this.statusLabel.Text = "Execution Successful";
                        this.statusLabel.Visible = true;
                        this.statusLabel.ForeColor = Color.ForestGreen;

                        float preferredWidths = this.resultGridView.Columns.OfType<DataGridViewColumn>().Sum(column =>
                            column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true));

                        foreach (var column in this.resultGridView.Columns.OfType<DataGridViewColumn>())
                        {
                            var preferredWidth =
                                column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true);
                            column.FillWeight = preferredWidth /
                                                preferredWidths;
                        }
                    }
                    catch (MySqlException mse)
                    {
                        this.statusLabel.Text = "Execution Failed";
                        this.statusLabel.Visible = true;
                        this.statusLabel.ForeColor = Color.Red;
                        MessageBox.Show($"The following error ocurred :{mse.Message}");
                    }
                }
                conn.Close();
            }
        }
    }
}