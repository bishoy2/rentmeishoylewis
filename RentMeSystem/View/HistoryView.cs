﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using RentMeSystem.DAL;

namespace RentMeSystem.View
{
    public partial class HistoryView : Form
    {
        public HistoryView()
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.UpdateSource();
        }

        private void UpdateSource()
        {
            using (var conn = DatabaseConnection.GetConnection("MySqlDatabaseConnection"))
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    try
                    {
                        command.CommandText =
                            "SELECT MemberID, Furniture_ItemNumber, CONCAT(fname, ' ', lname) AS MemberFullName, Employee_EmployeeID, TransactionDate, DueDate FROM `Transaction` JOIN Transaction_has_Furniture ON Transaction_has_Furniture.Transaction_TransactionID = `Transaction`.TransactionID JOIN Member ON Member.MemberID = `Transaction`.Member_MemberID JOIN Furniture ON Furniture.ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber JOIN Person ON Person.PersonID = Member.Person_PersonID;";
                        var dataAdapter = new MySqlDataAdapter(command);
                        var dataTable = new DataTable();
                        dataAdapter.Fill(dataTable);
                        this.rentalGridView.DataSource = dataTable;
                    }
                    catch (MySqlException mse)
                    {
                        MessageBox.Show($"The following error ocurred :{mse.Message}");
                    }
                }
                conn.Close();
            }

            using (var conn = DatabaseConnection.GetConnection("MySqlDatabaseConnection"))
            {
                conn.Open();
                using (var command = conn.CreateCommand())
                {
                    try
                    {
                        command.CommandText =
                            "SELECT MemberID, Furniture_ItemNumber, CONCAT(fname, ' ', lname) AS MemberFullName, `Return`.Employee_EmployeeID, ReturnDate FROM `Transaction` JOIN Transaction_has_Furniture ON Transaction_has_Furniture.Transaction_TransactionID = `Transaction`.TransactionID JOIN Member ON Member.MemberID = `Transaction`.Member_MemberID JOIN Furniture ON Furniture.ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber JOIN Person ON Person.PersonID = Member.Person_PersonID JOIN `Return` ON `Return`.Transaction_has_Furniture_Furniture_ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber AND `Return`.Transaction_has_Furniture_Transaction_TransactionID = Transaction_has_Furniture.Transaction_TransactionID;";
                        var dataAdapter = new MySqlDataAdapter(command);
                        var dataTable = new DataTable();
                        dataAdapter.Fill(dataTable);
                        this.returnlGridView.DataSource = dataTable;
                    }
                    catch (MySqlException mse)
                    {
                        MessageBox.Show($"The following error ocurred :{mse.Message}");
                    }
                }
                conn.Close();
            }
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            using (var conn = DatabaseConnection.GetConnection("MySqlDatabaseConnection"))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText =
                            "SELECT MemberID, Furniture_ItemNumber, CONCAT(fname, ' ', lname) AS MemberFullName, Employee_EmployeeID, TransactionDate FROM `Transaction` JOIN Transaction_has_Furniture ON Transaction_has_Furniture.Transaction_TransactionID = `Transaction`.TransactionID JOIN Member ON Member.MemberID = `Transaction`.Member_MemberID JOIN Furniture ON Furniture.ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber JOIN Person ON Person.PersonID = Member.Person_PersonID WHERE TransactionDate > @start AND TransactionDate < @end;";
                        cmd.Parameters.AddWithValue("@start", this.startDateTimePicker.Value);
                        cmd.Parameters.AddWithValue("@end", this.endDateTimePicker.Value);
                        var adapter = new MySqlDataAdapter(cmd);
                        var tbl = new DataTable();
                        adapter.Fill(tbl);
                        this.rentalGridView.DataSource = tbl;
                    }
                    catch (MySqlException mse)
                    {
                        MessageBox.Show($"The following error ocurred :{mse.Message}");
                    }
                }
                conn.Close();
            }

            this.returnlGridView.DataSource = null;
        }

        private void memberField_TextChanged(object sender, EventArgs e)
        {
            if (this.memberField.Text == "")
            {
                this.UpdateSource();
                return;
            }


            int memberID;

            if (!int.TryParse(this.memberField.Text, out memberID))
            {
                this.UpdateSource();
                return;
            }
            using (var conn = DatabaseConnection.GetConnection("MySqlDatabaseConnection"))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText =
                            "SELECT MemberID, Furniture_ItemNumber, CONCAT(fname, ' ', lname) AS MemberFullName, Employee_EmployeeID, TransactionDate, DueDate FROM `Transaction` JOIN Transaction_has_Furniture ON Transaction_has_Furniture.Transaction_TransactionID = `Transaction`.TransactionID JOIN Member ON Member.MemberID = `Transaction`.Member_MemberID JOIN Furniture ON Furniture.ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber JOIN Person ON Person.PersonID = Member.Person_PersonID WHERE MemberID = @memberid;";
                        cmd.Parameters.AddWithValue("@memberid", memberID);
                        var adapter = new MySqlDataAdapter(cmd);
                        var tbl = new DataTable();
                        adapter.Fill(tbl);
                        this.rentalGridView.DataSource = tbl;
                    }
                    catch (MySqlException mse)
                    {
                        MessageBox.Show($"The following error ocurred :{mse.Message}");
                    }
                }
                conn.Close();
            }

            using (var conn = DatabaseConnection.GetConnection("MySqlDatabaseConnection"))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText =
                            "SELECT MemberID, Furniture_ItemNumber, CONCAT(fname, ' ', lname) AS MemberFullName, `Return`.Employee_EmployeeID, ReturnDate FROM `Transaction` JOIN Transaction_has_Furniture ON Transaction_has_Furniture.Transaction_TransactionID = `Transaction`.TransactionID JOIN Member ON Member.MemberID = `Transaction`.Member_MemberID JOIN Furniture ON Furniture.ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber JOIN Person ON Person.PersonID = Member.Person_PersonID JOIN `Return` ON `Return`.Transaction_has_Furniture_Furniture_ItemNumber = Transaction_has_Furniture.Furniture_ItemNumber AND `Return`.Transaction_has_Furniture_Transaction_TransactionID = Transaction_has_Furniture.Transaction_TransactionID WHERE MemberId = @memberid";
                        cmd.Parameters.AddWithValue("@memberid", memberID);
                        var adapter = new MySqlDataAdapter(cmd);
                        var tbl = new DataTable();
                        adapter.Fill(tbl);
                        this.returnlGridView.DataSource = tbl;
                    }
                    catch (MySqlException mse)
                    {
                        var errorBox = MessageBox.Show("The following error ocurred :" + mse.Message);
                    }
                }
                conn.Close();
            }
        }

        private void startDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            this.endDateTimePicker.MinDate = this.startDateTimePicker.Value;
        }
    }
}