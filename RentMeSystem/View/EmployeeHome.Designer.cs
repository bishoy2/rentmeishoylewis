﻿namespace RentMeSystem.View
{
    partial class EmployeeHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logoutButton = new System.Windows.Forms.Button();
            this.registerButton = new System.Windows.Forms.Button();
            this.viewEditMemberButton = new System.Windows.Forms.Button();
            this.searchFurnitureButton = new System.Windows.Forms.Button();
            this.viewRentalsButton = new System.Windows.Forms.Button();
            this.usernamePanel = new System.Windows.Forms.Panel();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.queryInterfaceButton = new System.Windows.Forms.Button();
            this.transactionViewButton = new System.Windows.Forms.Button();
            this.usernamePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(664, 420);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(58, 29);
            this.logoutButton.TabIndex = 6;
            this.logoutButton.Text = "Log Out";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // registerButton
            // 
            this.registerButton.Location = new System.Drawing.Point(12, 47);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(185, 29);
            this.registerButton.TabIndex = 0;
            this.registerButton.Text = "Register Member";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // viewEditMemberButton
            // 
            this.viewEditMemberButton.Location = new System.Drawing.Point(12, 82);
            this.viewEditMemberButton.Name = "viewEditMemberButton";
            this.viewEditMemberButton.Size = new System.Drawing.Size(185, 29);
            this.viewEditMemberButton.TabIndex = 1;
            this.viewEditMemberButton.Text = "View And Edit Members";
            this.viewEditMemberButton.UseVisualStyleBackColor = true;
            this.viewEditMemberButton.Click += new System.EventHandler(this.viewEditMemberButton_Click);
            // 
            // searchFurnitureButton
            // 
            this.searchFurnitureButton.Location = new System.Drawing.Point(12, 117);
            this.searchFurnitureButton.Name = "searchFurnitureButton";
            this.searchFurnitureButton.Size = new System.Drawing.Size(185, 29);
            this.searchFurnitureButton.TabIndex = 2;
            this.searchFurnitureButton.Text = "Search And Rent Furniture";
            this.searchFurnitureButton.UseVisualStyleBackColor = true;
            this.searchFurnitureButton.Click += new System.EventHandler(this.searchFurnitureButton_Click);
            // 
            // viewRentalsButton
            // 
            this.viewRentalsButton.Location = new System.Drawing.Point(12, 152);
            this.viewRentalsButton.Name = "viewRentalsButton";
            this.viewRentalsButton.Size = new System.Drawing.Size(185, 29);
            this.viewRentalsButton.TabIndex = 3;
            this.viewRentalsButton.Text = "View Rentals And Return Furniture";
            this.viewRentalsButton.UseVisualStyleBackColor = true;
            this.viewRentalsButton.Click += new System.EventHandler(this.viewRentalsButton_Click);
            // 
            // usernamePanel
            // 
            this.usernamePanel.Controls.Add(this.usernameLabel);
            this.usernamePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.usernamePanel.Location = new System.Drawing.Point(0, 0);
            this.usernamePanel.Name = "usernamePanel";
            this.usernamePanel.Size = new System.Drawing.Size(734, 29);
            this.usernamePanel.TabIndex = 8;
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.usernameLabel.Location = new System.Drawing.Point(714, 0);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Padding = new System.Windows.Forms.Padding(10);
            this.usernameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.usernameLabel.Size = new System.Drawing.Size(20, 33);
            this.usernameLabel.TabIndex = 9;
            // 
            // queryInterfaceButton
            // 
            this.queryInterfaceButton.Location = new System.Drawing.Point(12, 222);
            this.queryInterfaceButton.Name = "queryInterfaceButton";
            this.queryInterfaceButton.Size = new System.Drawing.Size(185, 29);
            this.queryInterfaceButton.TabIndex = 5;
            this.queryInterfaceButton.Text = "Administrator Query Interface";
            this.queryInterfaceButton.UseVisualStyleBackColor = true;
            this.queryInterfaceButton.Click += new System.EventHandler(this.queryInterfaceButton_Click);
            // 
            // transactionViewButton
            // 
            this.transactionViewButton.Location = new System.Drawing.Point(12, 187);
            this.transactionViewButton.Name = "transactionViewButton";
            this.transactionViewButton.Size = new System.Drawing.Size(185, 29);
            this.transactionViewButton.TabIndex = 4;
            this.transactionViewButton.Text = "Transaction History";
            this.transactionViewButton.UseVisualStyleBackColor = true;
            this.transactionViewButton.Click += new System.EventHandler(this.transactionViewButton_Click);
            // 
            // EmployeeHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.transactionViewButton);
            this.Controls.Add(this.queryInterfaceButton);
            this.Controls.Add(this.usernamePanel);
            this.Controls.Add(this.viewRentalsButton);
            this.Controls.Add(this.searchFurnitureButton);
            this.Controls.Add(this.viewEditMemberButton);
            this.Controls.Add(this.registerButton);
            this.Controls.Add(this.logoutButton);
            this.Name = "EmployeeHome";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "EmployeeHome";
            this.usernamePanel.ResumeLayout(false);
            this.usernamePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.Button viewEditMemberButton;
        private System.Windows.Forms.Button searchFurnitureButton;
        private System.Windows.Forms.Button viewRentalsButton;
        private System.Windows.Forms.Panel usernamePanel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Button queryInterfaceButton;
        private System.Windows.Forms.Button transactionViewButton;
    }
}