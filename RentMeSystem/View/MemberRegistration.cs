﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using RentMeSystem.Controller;
using RentMeSystem.Model;

namespace RentMeSystem.View
{
    public partial class MemberRegistration : Form
    {
        private readonly Member _currentMember;
        private readonly bool _insert;
        private readonly MemberController _memberController;

        public MemberRegistration(Member currentMember = null)
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;

            this._memberController = new MemberController();
            this.genderDropdown.DropDownStyle = ComboBoxStyle.DropDownList;
            this.stateDropdown.DropDownStyle = ComboBoxStyle.DropDownList;

            if (currentMember == null)
            {
                this._currentMember = new Member();
                this.saveButton.Text = "Register";

                this._insert = true;
            }
            else
            {
                this._currentMember = currentMember;
                this.saveButton.Text = "Update";

                this.firstNameField.Text = currentMember.Person.FirstName;
                this.lastNameField.Text = currentMember.Person.LastName;
                this.ssnField.Text = currentMember.Person.Ssn;
                this.genderDropdown.SelectedIndex = this.genderDropdown.FindString(currentMember.Person.Gender);

                this.streetField.Text = currentMember.Street;
                this.cityField.Text = currentMember.City;
                this.zipField.Text = currentMember.Zip;
                this.stateDropdown.SelectedIndex = this.stateDropdown.FindString(currentMember.State);
                this.phoneField.Text = currentMember.Phone;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (this.IsValid())
            {
                this.UpdateSource();

                if (this._insert)
                    this.InsertMember();
                else
                    this.UpdateMember();
            }
            else
            {
                MessageBox.Show("Please fill out all forms correctly.");
            }
        }

        private void UpdateSource()
        {
            this._currentMember.Person.FirstName = this.firstNameField.Text;
            this._currentMember.Person.LastName = this.lastNameField.Text;
            this._currentMember.Person.Ssn = this.ssnField.Text;
            this._currentMember.Person.Gender = this.genderDropdown.Text;

            this._currentMember.Street = this.streetField.Text;
            this._currentMember.City = this.cityField.Text;
            this._currentMember.Zip = this.zipField.Text;
            this._currentMember.State = this.stateDropdown.Text;
            this._currentMember.Phone = this.phoneField.Text;
        }

        private bool IsValid()
        {
            var firstNameValid = !string.IsNullOrEmpty(this.firstNameField.Text.Trim());
            var lastNameValid = !string.IsNullOrEmpty(this.lastNameField.Text.Trim());
            var streetValid = !string.IsNullOrEmpty(this.streetField.Text.Trim());
            var cityValid = !string.IsNullOrEmpty(this.cityField.Text.Trim());
            var stateValid = !string.IsNullOrEmpty(this.stateDropdown.Text.Trim());
            var zipValid = !string.IsNullOrEmpty(this.zipField.Text.Trim());
            var phoneValid = !string.IsNullOrEmpty(this.phoneField.Text.Trim().Replace("-", "")) &&
                             this.phoneField.Text.Trim().Length >= 10 &&
                             this.phoneField.Text.Trim().Length <= 12;
            var genderValid = !string.IsNullOrEmpty(this.genderDropdown.Text.Trim());
            var ssnValid = !string.IsNullOrEmpty(this.ssnField.Text.Trim().Replace("-", "")) &&
                           this.ssnField.Text.Trim().Length >= 9 && this.ssnField.Text.Trim().Length <= 11;


            return firstNameValid && lastNameValid && streetValid && cityValid && stateValid && zipValid &&
                   phoneValid && genderValid && ssnValid;
        }

        private void UpdateMember()
        {
            try
            {
                this._memberController.Update(this._currentMember);
                MessageBox.Show("Member Information Successfully Updated!");
                this.Hide();
            }
            catch (MySqlException mse)
            {
                MessageBox.Show(mse.Message);
            }
        }

        private void InsertMember()
        {
            try
            {
                this._memberController.Add(this._currentMember);
                MessageBox.Show("Member Successfully Created!");
                this.Hide();
            }
            catch (MySqlException mse)
            {
                MessageBox.Show(mse.Message);
            }
        }
    }
}