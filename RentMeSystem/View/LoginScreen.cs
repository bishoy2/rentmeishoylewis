﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using RentMeSystem.Controller;

namespace RentMeSystem.View
{
    public partial class LoginScreen : Form
    {
        private readonly EmployeeController _controller;

        public LoginScreen()
        {
            this.InitializeComponent();
            this._controller = new EmployeeController();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._controller.Authenticate(this.idField.Text, this.passwordField.Text))
                {
                    this.Hide();

                    var id = int.Parse(this.idField.Text);

                    using (var employeeHome = new EmployeeHome(this._controller.GetById(id)))
                    {
                        employeeHome.ShowDialog();
                    }

                    this.Reset();

                    this.Show();
                }
                else
                {
                    MessageBox.Show("Login Failed!");
                }
            }
            catch (MySqlException mse)
            {
                MessageBox.Show(mse.Message);
            }
        }

        private void Reset()
        {
            this.idField.Clear();
            this.passwordField.Clear();
            this.ActiveControl = this.idField;
        }
    }
}