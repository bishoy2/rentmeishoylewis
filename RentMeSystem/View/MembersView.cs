﻿using System;
using System.Linq;
using System.Windows.Forms;
using RentMeSystem.Controller;
using RentMeSystem.Model;

namespace RentMeSystem.View
{
    public partial class MembersView : Form
    {
        private readonly MemberController _memberController;

        public MembersView()
        {
            this.InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;

            this._memberController = new MemberController();
            this.searchBox.DataSource = Enum.GetValues(typeof(SearchOptions));
            this.UpdateSource();
        }

        private void UpdateSource()
        {
            this.memberGridView.DataSource = this._memberController.GetAll().ToList();
            this.memberGridView.TopLeftHeaderCell.Value = "✎";

            float preferredWidths = this.memberGridView.Columns.OfType<DataGridViewColumn>().Sum(column =>
                column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true));

            foreach (var column in this.memberGridView.Columns.OfType<DataGridViewColumn>())
            {
                var preferredWidth =
                    column.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, true);
                column.FillWeight = preferredWidth /
                                    preferredWidths;
            }
        }

        private void memberGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dataBoundItem = this.memberGridView.Rows[e.RowIndex].DataBoundItem;

            using (var registration = new MemberRegistration(dataBoundItem as Member))
            {
                registration.ShowDialog();
            }

            this.Show();

            this.UpdateSource();
        }

        private void searchField_TextChanged(object sender, EventArgs e)
        {
            if (this.searchField.Text == string.Empty)
            {
                this.UpdateSource();
            }
            else
            {
                var currentSearchOption =
                    (SearchOptions) Enum.GetValues(typeof(SearchOptions)).GetValue(this.searchBox.SelectedIndex);
                var searchText = this.searchField.Text;
                switch (currentSearchOption)
                {
                    case SearchOptions.MemberID:
                        this.memberGridView.DataSource = this._memberController.GetByMemberId(searchText).ToList();
                        break;
                    case SearchOptions.FirstAndLast:
                        this.memberGridView.DataSource = this._memberController.GetByName(searchText).ToList();
                        break;
                    case SearchOptions.FirstName:
                        this.memberGridView.DataSource = this._memberController.GetByFirst(searchText).ToList();
                        break;
                    case SearchOptions.Gender:
                        this.memberGridView.DataSource = this._memberController.GetByGender(searchText).ToList();
                        break;
                    case SearchOptions.LastName:
                        this.memberGridView.DataSource = this._memberController.GetByLast(searchText).ToList();
                        break;
                    case SearchOptions.SSN:
                        this.memberGridView.DataSource = this._memberController.GetByMemberSsn(searchText).ToList();
                        break;
                    case SearchOptions.Street:
                        this.memberGridView.DataSource = this._memberController.GetByStreet(searchText).ToList();
                        break;
                    case SearchOptions.City:
                        this.memberGridView.DataSource = this._memberController.GetByCity(searchText).ToList();
                        break;
                    case SearchOptions.Zip:
                        this.memberGridView.DataSource = this._memberController.GetByZip(searchText).ToList();
                        break;
                    case SearchOptions.State:
                        this.memberGridView.DataSource = this._memberController.GetByState(searchText).ToList();
                        break;
                    case SearchOptions.PhoneNumber:
                        this.memberGridView.DataSource = this._memberController.GetByPhoneNumber(searchText).ToList();
                        break;
                    default:
                        break;
                }
            }
        }

        private void searchBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.searchField.Text = string.Empty;
        }

        private enum SearchOptions
        {
            MemberID,
            FirstAndLast,
            FirstName,
            LastName,
            SSN,
            Gender,
            Street,
            City,
            State,
            Zip,
            PhoneNumber
        }
    }
}