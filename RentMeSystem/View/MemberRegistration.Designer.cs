﻿namespace RentMeSystem.View
{
    partial class MemberRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.phoneNumberLbl = new System.Windows.Forms.Label();
            this.phoneField = new System.Windows.Forms.TextBox();
            this.genderLbl = new System.Windows.Forms.Label();
            this.genderDropdown = new System.Windows.Forms.ComboBox();
            this.zipLbl = new System.Windows.Forms.Label();
            this.zipField = new System.Windows.Forms.TextBox();
            this.stateLbl = new System.Windows.Forms.Label();
            this.stateDropdown = new System.Windows.Forms.ComboBox();
            this.cityLbl = new System.Windows.Forms.Label();
            this.cityField = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.ssnField = new System.Windows.Forms.TextBox();
            this.ssnLbl = new System.Windows.Forms.Label();
            this.streetField = new System.Windows.Forms.TextBox();
            this.streetLbl = new System.Windows.Forms.Label();
            this.lastNameField = new System.Windows.Forms.TextBox();
            this.lastNameLbl = new System.Windows.Forms.Label();
            this.firstNameField = new System.Windows.Forms.TextBox();
            this.firstNameLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // phoneNumberLbl
            // 
            this.phoneNumberLbl.AutoSize = true;
            this.phoneNumberLbl.Location = new System.Drawing.Point(34, 136);
            this.phoneNumberLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.phoneNumberLbl.Name = "phoneNumberLbl";
            this.phoneNumberLbl.Size = new System.Drawing.Size(38, 13);
            this.phoneNumberLbl.TabIndex = 57;
            this.phoneNumberLbl.Text = "Phone";
            // 
            // phoneField
            // 
            this.phoneField.Location = new System.Drawing.Point(76, 133);
            this.phoneField.Margin = new System.Windows.Forms.Padding(2);
            this.phoneField.Name = "phoneField";
            this.phoneField.Size = new System.Drawing.Size(82, 20);
            this.phoneField.TabIndex = 7;
            // 
            // genderLbl
            // 
            this.genderLbl.AutoSize = true;
            this.genderLbl.Location = new System.Drawing.Point(30, 35);
            this.genderLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.genderLbl.Name = "genderLbl";
            this.genderLbl.Size = new System.Drawing.Size(42, 13);
            this.genderLbl.TabIndex = 55;
            this.genderLbl.Text = "Gender";
            // 
            // genderDropdown
            // 
            this.genderDropdown.FormattingEnabled = true;
            this.genderDropdown.Items.AddRange(new object[] {
            "",
            "Male",
            "Female",
            "Unknown",
            "Other"});
            this.genderDropdown.Location = new System.Drawing.Point(76, 32);
            this.genderDropdown.Margin = new System.Windows.Forms.Padding(2);
            this.genderDropdown.Name = "genderDropdown";
            this.genderDropdown.Size = new System.Drawing.Size(82, 21);
            this.genderDropdown.TabIndex = 2;
            // 
            // zipLbl
            // 
            this.zipLbl.AutoSize = true;
            this.zipLbl.Location = new System.Drawing.Point(198, 111);
            this.zipLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.zipLbl.Name = "zipLbl";
            this.zipLbl.Size = new System.Drawing.Size(22, 13);
            this.zipLbl.TabIndex = 53;
            this.zipLbl.Text = "Zip";
            // 
            // zipField
            // 
            this.zipField.Location = new System.Drawing.Point(224, 108);
            this.zipField.Margin = new System.Windows.Forms.Padding(2);
            this.zipField.Name = "zipField";
            this.zipField.Size = new System.Drawing.Size(82, 20);
            this.zipField.TabIndex = 6;
            // 
            // stateLbl
            // 
            this.stateLbl.AutoSize = true;
            this.stateLbl.Location = new System.Drawing.Point(40, 111);
            this.stateLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.stateLbl.Name = "stateLbl";
            this.stateLbl.Size = new System.Drawing.Size(32, 13);
            this.stateLbl.TabIndex = 51;
            this.stateLbl.Text = "State";
            // 
            // stateDropdown
            // 
            this.stateDropdown.FormattingEnabled = true;
            this.stateDropdown.Items.AddRange(new object[] {
            "",
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"});
            this.stateDropdown.Location = new System.Drawing.Point(76, 108);
            this.stateDropdown.Margin = new System.Windows.Forms.Padding(2);
            this.stateDropdown.Name = "stateDropdown";
            this.stateDropdown.Size = new System.Drawing.Size(82, 21);
            this.stateDropdown.TabIndex = 5;
            // 
            // cityLbl
            // 
            this.cityLbl.AutoSize = true;
            this.cityLbl.Location = new System.Drawing.Point(48, 87);
            this.cityLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cityLbl.Name = "cityLbl";
            this.cityLbl.Size = new System.Drawing.Size(24, 13);
            this.cityLbl.TabIndex = 49;
            this.cityLbl.Text = "City";
            // 
            // cityField
            // 
            this.cityField.Location = new System.Drawing.Point(76, 84);
            this.cityField.Margin = new System.Windows.Forms.Padding(2);
            this.cityField.Name = "cityField";
            this.cityField.Size = new System.Drawing.Size(82, 20);
            this.cityField.TabIndex = 4;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(224, 222);
            this.saveButton.Margin = new System.Windows.Forms.Padding(2);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(82, 28);
            this.saveButton.TabIndex = 9;
            this.saveButton.Text = "Register";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // ssnField
            // 
            this.ssnField.Location = new System.Drawing.Point(224, 133);
            this.ssnField.Margin = new System.Windows.Forms.Padding(2);
            this.ssnField.Name = "ssnField";
            this.ssnField.Size = new System.Drawing.Size(82, 20);
            this.ssnField.TabIndex = 8;
            // 
            // ssnLbl
            // 
            this.ssnLbl.AutoSize = true;
            this.ssnLbl.Location = new System.Drawing.Point(191, 136);
            this.ssnLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ssnLbl.Name = "ssnLbl";
            this.ssnLbl.Size = new System.Drawing.Size(29, 13);
            this.ssnLbl.TabIndex = 45;
            this.ssnLbl.Text = "SSN";
            // 
            // streetField
            // 
            this.streetField.Location = new System.Drawing.Point(76, 57);
            this.streetField.Margin = new System.Windows.Forms.Padding(2);
            this.streetField.Name = "streetField";
            this.streetField.Size = new System.Drawing.Size(230, 20);
            this.streetField.TabIndex = 3;
            // 
            // streetLbl
            // 
            this.streetLbl.AutoSize = true;
            this.streetLbl.Location = new System.Drawing.Point(37, 60);
            this.streetLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.streetLbl.Name = "streetLbl";
            this.streetLbl.Size = new System.Drawing.Size(35, 13);
            this.streetLbl.TabIndex = 43;
            this.streetLbl.Text = "Street";
            // 
            // lastNameField
            // 
            this.lastNameField.Location = new System.Drawing.Point(224, 8);
            this.lastNameField.Margin = new System.Windows.Forms.Padding(2);
            this.lastNameField.Name = "lastNameField";
            this.lastNameField.Size = new System.Drawing.Size(82, 20);
            this.lastNameField.TabIndex = 1;
            // 
            // lastNameLbl
            // 
            this.lastNameLbl.AutoSize = true;
            this.lastNameLbl.Location = new System.Drawing.Point(162, 11);
            this.lastNameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lastNameLbl.Name = "lastNameLbl";
            this.lastNameLbl.Size = new System.Drawing.Size(58, 13);
            this.lastNameLbl.TabIndex = 41;
            this.lastNameLbl.Text = "Last Name";
            // 
            // firstNameField
            // 
            this.firstNameField.Location = new System.Drawing.Point(76, 8);
            this.firstNameField.Margin = new System.Windows.Forms.Padding(2);
            this.firstNameField.Name = "firstNameField";
            this.firstNameField.Size = new System.Drawing.Size(82, 20);
            this.firstNameField.TabIndex = 0;
            // 
            // firstNameLbl
            // 
            this.firstNameLbl.AutoSize = true;
            this.firstNameLbl.Location = new System.Drawing.Point(15, 11);
            this.firstNameLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.firstNameLbl.Name = "firstNameLbl";
            this.firstNameLbl.Size = new System.Drawing.Size(57, 13);
            this.firstNameLbl.TabIndex = 39;
            this.firstNameLbl.Text = "First Name";
            // 
            // MemberRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 261);
            this.Controls.Add(this.phoneNumberLbl);
            this.Controls.Add(this.phoneField);
            this.Controls.Add(this.genderLbl);
            this.Controls.Add(this.genderDropdown);
            this.Controls.Add(this.zipLbl);
            this.Controls.Add(this.zipField);
            this.Controls.Add(this.stateLbl);
            this.Controls.Add(this.stateDropdown);
            this.Controls.Add(this.cityLbl);
            this.Controls.Add(this.cityField);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.ssnField);
            this.Controls.Add(this.ssnLbl);
            this.Controls.Add(this.streetField);
            this.Controls.Add(this.streetLbl);
            this.Controls.Add(this.lastNameField);
            this.Controls.Add(this.lastNameLbl);
            this.Controls.Add(this.firstNameField);
            this.Controls.Add(this.firstNameLbl);
            this.Name = "MemberRegistration";
            this.Text = "MemberRegistration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label phoneNumberLbl;
        private System.Windows.Forms.TextBox phoneField;
        private System.Windows.Forms.Label genderLbl;
        private System.Windows.Forms.ComboBox genderDropdown;
        private System.Windows.Forms.Label zipLbl;
        private System.Windows.Forms.TextBox zipField;
        private System.Windows.Forms.Label stateLbl;
        private System.Windows.Forms.ComboBox stateDropdown;
        private System.Windows.Forms.Label cityLbl;
        private System.Windows.Forms.TextBox cityField;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox ssnField;
        private System.Windows.Forms.Label ssnLbl;
        private System.Windows.Forms.TextBox streetField;
        private System.Windows.Forms.Label streetLbl;
        private System.Windows.Forms.TextBox lastNameField;
        private System.Windows.Forms.Label lastNameLbl;
        private System.Windows.Forms.TextBox firstNameField;
        private System.Windows.Forms.Label firstNameLbl;
    }
}